## Overview ##

This plugin makes it easy to send messages to a Microsoft Teams Channel through the Q-SYS system.

## Controls ##

**Send** (Trigger) Sends the message  
**Title** The title shown in the message  
**Body** The body of the message  
**Webhook URL** The link that tells Teams where to send your message  

## How to get the webhook URL ##
Take a look at [this page](https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook) for information about Teams incoming webhooks. The third section gives the instructions you need to set up the webhook and retrieve the URL.

## Contact ##
For questions, suggestions, or comments, please contact me through the message system here on Communities.